<?php

namespace Dedermus\Admin\Layout;

interface Buildable
{
    public function build();
}

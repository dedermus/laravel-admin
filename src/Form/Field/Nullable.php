<?php

namespace Dedermus\Admin\Form\Field;

use Dedermus\Admin\Form\Field;

class Nullable extends Field
{
    public function __construct()
    {
    }

    public function __call($method, $parameters)
    {
        return $this;
    }
}

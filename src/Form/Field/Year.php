<?php

namespace Dedermus\Admin\Form\Field;

class Year extends Date
{
    protected $format = 'YYYY';
}
